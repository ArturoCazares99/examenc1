package com.example.examen;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc){
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public ReciboNomina(){
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public double calcularSubtotal(){
        if(puesto == 1){
            return (50 * horasTrabNormal)+(100 * horasTrabExtras);
        }
        else if(puesto == 2){
            return (70 * horasTrabNormal)+(140 * horasTrabExtras);
        }
        else{
            return (100 * horasTrabNormal)+(200 * horasTrabExtras);
        }
    }

    public double calcularImpuesto(){
        return calcularSubtotal()*0.16;
    }

    public double calcularTotal(){
        return calcularSubtotal() - calcularImpuesto();
    }
}
